#ifndef OCLDRIVER_H
#define OCLDRIVER_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "CL/cl.h"

#define OCLDRIVER_SUCCESS  0
#define OCLDRIVER_FATAL   -1

struct oclGlobalHandle;
struct oclPlatformHandle;

struct oclDeviceHandle {
	struct oclPlatformHandle *phandle;
	cl_device_id			   cl_id;
	int 					   ocl_device_id;
	bool 					   available;
	char					 * name;
	cl_ulong				   global_mem_size;
};

struct oclPlatformHandle {
	struct oclGlobalHandle * ghandle;
	char  		           * name;
	cl_platform_id           cl_id;
	int 					 ocl_platform_id;
	cl_uint			         num_devices;
	cl_device_id           * device_ids;
	struct oclDeviceHandle * dhandles;
};

struct oclGlobalHandle {
	cl_uint                    num_platforms;
	cl_platform_id			 * platform_ids;
	struct oclPlatformHandle * phandles;
};


int ocldriver_globalHandleInit(struct oclGlobalHandle * handle);

#endif // OCLDRIVER_H
