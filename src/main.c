#include <stdio.h>
#include "ocldriver.h"


int main(int argc, char **argv) {
	struct oclGlobalHandle * ghandle = calloc(1, sizeof(struct oclGlobalHandle));
	if (!ghandle)
		return -1;

	int rc =  ocldriver_globalHandleInit(ghandle);
	return rc;
}
