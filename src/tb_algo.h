#ifndef _TB_ALGO_H
#define _TB_ALGO_H

#define TB_ALGO_VERSION 1

enum tb_algo_type {
	eTB_ALGO_CPU_CUSTOM,
	eTB_ALGO_CPU_OCL,
	eTB_ALGO_GPU_OCL,
	eTB_ALGO_GPU_ASM,
};

struct tb_algo_header_t {
	uint32_t version;
	const char  * name;
	enum          tb_algo_type;
	uint32_t	  local_worksize;
	uint32_t      global_worksize;
	uint32_t 	  sw_worksize;
	void        * payload;
};


struct custom_cpu_payload_t {
	int (*hash)(uint8_t * output, uint8_t *input, size_t isize);
};








#endif // _TB_ALGO_H
