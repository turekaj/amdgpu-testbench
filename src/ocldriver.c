#include "ocldriver.h"
#include <stdio.h>


void ocldriver_devicePrintInfo(struct oclDeviceHandle *handle) {
	printf("%s: name:%s ocl_device_id:%d available:%d global_mem_size:%lu\n", __func__, 
			    														     handle->name,
			    														     handle->ocl_device_id,
			    														     handle->available,
			    														     handle->global_mem_size);
}

void ocldriver_platformPrintInfo(struct oclPlatformHandle *handle) {
	printf("%s: name:%s ocl_platform_id:%d num_devices:%d\n", __func__,
															  handle->name,
															  handle->ocl_platform_id,
															  handle->num_devices);
}

int ocldriver_deviceHandleInit(struct oclDeviceHandle *handle, struct oclPlatformHandle *phandle, cl_device_id device_id, int ocl_device_id) {
	handle->phandle = phandle;
	handle->cl_id   = device_id;
	handle->ocl_device_id = ocl_device_id;
	
	size_t name_size = 0;
	cl_int err = clGetDeviceInfo(handle->cl_id, CL_DEVICE_NAME, 0, NULL, &name_size);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to query device name size for platform %s (%d) device %d\n", __func__, handle->phandle->name, handle->phandle->ocl_platform_id, handle->ocl_device_id);
		return OCLDRIVER_FATAL;
	}

	handle->name = (char *) malloc(name_size);
	if (!handle->name) {
		printf("%s: Failed to allocate %lu bytes for  device name platform %s (%d) device %d\n", __func__, name_size, handle->phandle->name, handle->phandle->ocl_platform_id, handle->ocl_device_id);
		return OCLDRIVER_FATAL;
	}

	err = clGetDeviceInfo(handle->cl_id, CL_DEVICE_NAME, name_size, handle->name, NULL);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to query device name for platform %s (%d) device %d\n", __func__, handle->phandle->name, handle->phandle->ocl_platform_id, handle->ocl_device_id);
		free(handle->name);
		return OCLDRIVER_FATAL;
	}


	err = clGetDeviceInfo(handle->cl_id, CL_DEVICE_AVAILABLE, sizeof(cl_bool), &handle->available, NULL);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to query device availability for platform %s (%d) device %s (%d)\n", __func__, handle->phandle->name, handle->phandle->ocl_platform_id, handle->name, handle->ocl_device_id); 
		free(handle->name);
		return OCLDRIVER_FATAL;
	}

	err = clGetDeviceInfo(handle->cl_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &handle->global_mem_size, NULL);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to query device global_mem_size for platform %s (%d) device %s (%d)\n", __func__, handle->phandle->name, handle->phandle->ocl_platform_id, handle->name, handle->ocl_device_id); 
		free(handle->name);
		return OCLDRIVER_FATAL;
	}
	
	ocldriver_devicePrintInfo(handle);
	
	return OCLDRIVER_SUCCESS;
}

int ocldriver_platformHandleInit(struct oclPlatformHandle *handle, struct oclGlobalHandle *ghandle, cl_platform_id platform_id, int ocl_platform_id) {
	handle->ghandle  = ghandle;
	handle->cl_id = platform_id;
	handle->ocl_platform_id = ocl_platform_id;

	size_t name_size = 0;

	cl_int err = clGetPlatformInfo(handle->cl_id, CL_PLATFORM_NAME, 0, NULL, &name_size);
	if (err != CL_SUCCESS)  {
		printf("%s:Failed to fetch platform name size for platform_id %d\n", __func__, handle->ocl_platform_id);
		return OCLDRIVER_FATAL;
	}

	handle->name = (char *) malloc(name_size);
	if (!handle->name) {
		printf("%s: Failed to allocate %lu bytes for handle->name\n", __func__, name_size);
		return OCLDRIVER_FATAL;
	}
	
	err = clGetPlatformInfo(handle->cl_id, CL_PLATFORM_NAME, name_size, handle->name, NULL);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to fetch name for platform %d\n", __func__, handle->ocl_platform_id);
		free(handle->name);
		return OCLDRIVER_FATAL;
	}

	err = clGetDeviceIDs(handle->cl_id, CL_DEVICE_TYPE_ALL, 0, NULL, &handle->num_devices);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to query number of devices for platform %s (%d) \n", __func__, handle->name, handle->ocl_platform_id);
		free(handle->name);
		return OCLDRIVER_FATAL;
	}

	if (handle->num_devices == 0) {
		printf("%s: Did not detect any devices for platform %s (%d)\n", __func__, handle->name, handle->ocl_platform_id);
		free(handle->name);
		return OCLDRIVER_FATAL;
	}

	handle->device_ids = calloc(handle->num_devices, sizeof(cl_device_id));
	if (!handle->device_ids) {
		printf("%s: Failed to allocate memory for %d devices platform %s (%d)\n", __func__, handle->num_devices, handle->name, handle->ocl_platform_id);
		free(handle->name);
		return OCLDRIVER_FATAL;
	}

	err = clGetDeviceIDs(handle->cl_id, CL_DEVICE_TYPE_ALL, handle->num_devices, handle->device_ids, 0);
	if (err != CL_SUCCESS) {
		printf("%s: Failed to query device IDs for platform %s (%d)\n", __func__, handle->name, handle->ocl_platform_id);
		free(handle->name);
		free(handle->device_ids);
		return OCLDRIVER_FATAL;
	}

	handle->dhandles = calloc(handle->num_devices, sizeof(struct oclDeviceHandle));
	if (!handle->dhandles) {
		printf("%s: Failed to allocate memory for %d oclDeviceHandles for platform %s (%d)\n", __func__, handle->num_devices, handle->name, handle->ocl_platform_id);
		free(handle->name);
		free(handle->device_ids);
		return OCLDRIVER_FATAL;
	}
	
	ocldriver_platformPrintInfo(handle);

	for (size_t index=0; index < handle->num_devices; index++) {
		int rc = ocldriver_deviceHandleInit(&handle->dhandles[index], handle, handle->device_ids[index], index);
		if (rc != OCLDRIVER_SUCCESS) {
			free(handle->name);
			free(handle->device_ids);
			free(handle->dhandles);
			return rc;
		}
	}

	return OCLDRIVER_SUCCESS;
}


int ocldriver_globalHandleInit(struct oclGlobalHandle * handle) {
	if (!handle)
		return OCLDRIVER_FATAL;

	cl_int err = clGetPlatformIDs(0, NULL, &handle->num_platforms);
	if (err != CL_SUCCESS) {
		printf("%s:Could not find OpenCL installation!\n", __func__);
		return OCLDRIVER_FATAL;
	}

	if (handle->num_platforms == 0) {
		printf("%s:Could not find any OpenCL platforms!\n", __func__);
		return OCLDRIVER_FATAL;
	}

	handle->platform_ids = calloc(handle->num_platforms, sizeof(cl_platform_id));
	if (!handle->platform_ids) {
		printf("%s:Failed to allocate memory for %d cl_platform_ids\n", __func__, handle->num_platforms);
		return OCLDRIVER_FATAL;
	}

	err = clGetPlatformIDs(handle->num_platforms, handle->platform_ids, NULL);
	if (err != CL_SUCCESS) {
		free(handle->platform_ids);
		return OCLDRIVER_FATAL;
	}

	handle->phandles = calloc(handle->num_platforms, sizeof(struct oclPlatformHandle));
	if (!handle->phandles) {
		printf("%s:Failed to allocate memory for %d oclPlatformHandles\n", __func__, handle->num_platforms);
		free(handle->platform_ids);
		return OCLDRIVER_FATAL;
	}
	
	for (size_t index=0; index < handle->num_platforms; index++) {
		int rc = ocldriver_platformHandleInit(&handle->phandles[index], handle, handle->platform_ids[index], index);
		if (rc != OCLDRIVER_SUCCESS) {
			free(handle->platform_ids);
			free(handle->phandles);
			return rc;
		}
	}

	return OCLDRIVER_SUCCESS;;
}

